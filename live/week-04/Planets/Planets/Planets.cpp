#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

int Add(int a, int b)
{
	return a + b;
}

TEST_CASE("Add is computed", "[Add]") {
    REQUIRE(Add(2, 2) == 4);
    REQUIRE(Add(2, 6) == 7);
}

